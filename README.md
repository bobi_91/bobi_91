# bobi_91
<h1 align="center">Hi 👋, I'm Borislav</h1>
<h3 align="center">⚡I have successfully completed the Telerik Alpha program for .NET developers. And I am ready to share my passion for programming with you!</h3>

- 🔭 I’m currently working on [Virtual Wallet](will be available soon) 

- 🌱 I’m currently learning **Docker**.

- 📫 How to reach me **borislavpenchev91@mail.com**

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> <a href="https://www.w3schools.com/cs/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg" alt="csharp" width="40" height="40"/> </a> <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://dotnet.microsoft.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/dot-net/dot-net-original-wordmark.svg" alt="dotnet" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.microsoft.com/en-us/sql-server" target="_blank" rel="noreferrer"> <img src="https://www.svgrepo.com/show/303229/microsoft-sql-server-logo.svg" alt="mssql" width="40" height="40"/> </a> <a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> </p>

<h3 align="left">Certifications & Badges:</h3>
[Certificate.pdf](https://gitlab.com/bobi_91/bobi_91/-/blob/main/Certificate.pdf?ref_type=heads)

### Projects that I am part of:

| Project | Position | Timespan |
|---------|----------|----------|
| VirtualWallet | Owner | since 10/2023 |
| Carpooling | Owner | 07/2023 - 08/2023 |
| Forum System | Owner | 06/2023 - 07/2023 |
| Task Management System | Owner | 04/2023 - 05/2023 |

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/borislav-penchev-852a2726b/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="borislav penchev" height="30" width="40" /></a>
</p>


